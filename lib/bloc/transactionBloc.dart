import 'dart:async';

import 'package:expense_manager/model/transaction.dart';
import 'package:expense_manager/repository/transactionRepository.dart';

class TransactionBloc {
  //Get instance of the Repository
  final _repository = TransactionRepository();

  //Stream controller is the 'Admin' that manages
  //the state of our stream of data like adding
  //new data, change the state of the stream
  //and broadcast it to observers/subscribers
  final _controller = StreamController<List<Transaction>>.broadcast();

  get todos => _controller.stream;

  TransactionBloc() {
    getTransaction();
  }

  getTransaction({String query}) async {
    //sink is a way of adding data reactively to the stream
    //by registering a new event
    _controller.sink.add(await _repository.getAllTransaction(query: query));
  }

  getSingleTransaction(int id) async {
    // _controller.sink.add(await _repository.getSingleTr/ansaction(id));
    return await _repository.getSingleTransaction(id);
  }

  addTransaction(Transaction transaction) async {
    await _repository.insertTransaction(transaction);
    getTransaction();
  }

  updateTransaction(Transaction transaction) async {
    await _repository.updateTransaction(transaction);
    getTransaction();
  }

  deleteTransactionById(int id) async {
    _repository.deleteTransactionById(id);
    getTransaction();
  }

  dispose() {
    _controller.close();
  }
}
