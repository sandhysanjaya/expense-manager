import 'dart:async';

import 'package:expense_manager/model/category.dart';
import 'package:expense_manager/repository/categoryRepository.dart';

class CategoryBloc {
  //Get instance of the Repository
  final _repository = CategoryRepository();

  //Stream controller is the 'Admin' that manages
  //the state of our stream of data like adding
  //new data, change the state of the stream
  //and broadcast it to observers/subscribers
  final _controller = StreamController<List<Category>>.broadcast();

  get category => _controller.stream;

  CategoryBloc() {
    getCategory();
  }

  getCategory({String query}) async {
    //sink is a way of adding data reactively to the stream
    //by registering a new event
    _controller.sink.add(await _repository.getAllCategory(query: query));
  }

  addCategory(Category category) async {
    await _repository.insertCategory(category);
    getCategory();
  }

  updateCategory(Category category) async {
    await _repository.updateCategory(category);
    getCategory();
  }

  deleteCategoryById(int id) async {
    _repository.deleteCategoryById(id);
    getCategory();
  }

  dispose() {
    _controller.close();
  }
}
