import 'package:expense_manager/database/database.dart';
import 'package:expense_manager/model/category.dart';

class CategoryDao {
  final dbProvider = DatabaseProvider.dbProvider;

  //Adds new Todo records
  Future<int> createTodo(Category category) async {
    final db = await dbProvider.database;
    var result = db.insert(categoryTABLE, category.toDatabaseJson());
    return result;
  }

  //Get All Todo items
  //Searches if query string was passed
  Future<List<Category>> getTodos({List<String> columns, String query}) async {
    final db = await dbProvider.database;

    List<Map<String, dynamic>> result;
    if (query != null) {
      if (query.isNotEmpty)
        result = await db.query(categoryTABLE,
            columns: columns,
            where: 'description LIKE ?',
            whereArgs: ["%$query%"]);
    } else {
      result = await db.query(categoryTABLE, columns: columns);
    }

    List<Category> todos = result.isNotEmpty
        ? result.map((item) => Category.fromDatabaseJson(item)).toList()
        : [];
    return todos;
  }

  //Update Todo record
  Future<int> updateTodo(Category category) async {
    final db = await dbProvider.database;

    var result = await db.update(categoryTABLE, category.toDatabaseJson(),
        where: "id = ?", whereArgs: [category.id]);

    return result;
  }

  //Delete Todo records
  Future<int> deleteTodo(int id) async {
    final db = await dbProvider.database;
    var result =
        await db.delete(categoryTABLE, where: 'id = ?', whereArgs: [id]);

    return result;
  }

  //We are not going to use this in the demo
  Future deleteAllTodos() async {
    final db = await dbProvider.database;
    var result = await db.delete(
      categoryTABLE,
    );

    return result;
  }
}
