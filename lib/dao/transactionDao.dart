import 'package:expense_manager/database/database.dart';
import 'package:expense_manager/model/transaction.dart';

class TransactionDao {
  final dbProvider = DatabaseProvider.dbProvider;

  //Adds new Todo records
  Future<int> createTodo(Transaction transaction) async {
    final db = await dbProvider.database;
    var result = db.insert(trxTABLE, transaction.toDatabaseJson());
    return result;
  }

  //Get All Todo items
  //Searches if query string was passed
  Future<List<Transaction>> getTodos(
      {List<String> columns, String query}) async {
    final db = await dbProvider.database;

    List<Map<String, dynamic>> result;
    if (query != null) {
      if (query.isNotEmpty)
        result = await db.query(trxTABLE,
            columns: columns, where: 'id = ? ', whereArgs: ["%$query%"]);
    } else {
      result = await db.query(trxTABLE, columns: columns);
    }

    List<Transaction> todos = result.isNotEmpty
        ? result.map((item) => Transaction.fromDatabaseJson(item)).toList()
        : [];
    return todos;
  }

  Future<Transaction> getSingleTransaction(int id) async {
    final db = await dbProvider.database;

    List<Map<String, dynamic>> result;
    result = await db.query(trxTABLE, where: 'id = ? ', whereArgs: ["$id"]);
    Transaction todos = result.isNotEmpty
        ? result
            .map((item) => Transaction.fromDatabaseJson(item))
            .toList()
            .elementAt(0)
        : [];
    return todos;
  }

  //Update Todo record
  Future<int> updateTodo(Transaction transaction) async {
    final db = await dbProvider.database;

    var result = await db.update(trxTABLE, transaction.toDatabaseJson(),
        where: "id = ?", whereArgs: [transaction.id]);

    return result;
  }

  //Delete Todo records
  Future<int> deleteTodo(int id) async {
    final db = await dbProvider.database;
    var result = await db.delete(trxTABLE, where: 'id = ?', whereArgs: [id]);

    return result;
  }

  //We are not going to use this in the demo
  Future deleteAllTodos() async {
    final db = await dbProvider.database;
    var result = await db.delete(
      trxTABLE,
    );

    return result;
  }
}
