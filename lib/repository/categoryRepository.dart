import 'package:expense_manager/dao/categoryDao.dart';
import 'package:expense_manager/model/category.dart';

class CategoryRepository {
  final dao = CategoryDao();

  Future getAllCategory({String query}) => dao.getTodos(query: query);

  Future insertCategory(Category category) => dao.createTodo(category);

  Future updateCategory(Category category) => dao.updateTodo(category);

  Future deleteCategoryById(int id) => dao.deleteTodo(id);

  //We are not going to use this in the demo
  Future deleteAllCategory() => dao.deleteAllTodos();
}
