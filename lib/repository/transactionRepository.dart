import 'package:expense_manager/dao/transactionDao.dart';
import 'package:expense_manager/model/transaction.dart';

class TransactionRepository {
  final dao = TransactionDao();

  Future getAllTransaction({String query}) => dao.getTodos(query: query);

  Future getSingleTransaction(int id) => dao.getSingleTransaction(id);

  Future insertTransaction(Transaction transaction) =>
      dao.createTodo(transaction);

  Future updateTransaction(Transaction transaction) =>
      dao.updateTodo(transaction);

  Future deleteTransactionById(int id) => dao.deleteTodo(id);

  //We are not going to use this in the demo
  Future deleteAllTransaction() => dao.deleteAllTodos();
}
