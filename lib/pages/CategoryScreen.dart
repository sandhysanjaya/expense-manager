import 'package:expense_manager/bloc/categoryBloc.dart';
import 'package:expense_manager/model/category.dart';
import 'package:flutter/material.dart';

class CategoryScreen extends StatefulWidget {
  @override
  _CategoryScreenState createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> {
  final CategoryBloc categoryBloc = CategoryBloc();

  _showAddCategorySheet(BuildContext context) {
    final _todoDescriptionFormController = TextEditingController();
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Create Category'),
            content: new Row(
              children: [
                new Expanded(
                    child: TextFormField(
                  controller: _todoDescriptionFormController,
                  textInputAction: TextInputAction.newline,
                  style: TextStyle(fontSize: 21, fontWeight: FontWeight.w400),
                  autofocus: true,
                  decoration: const InputDecoration(
                      hintText: 'Category Name',
                      labelStyle: TextStyle(
                          color: Colors.indigoAccent,
                          fontWeight: FontWeight.w500)),
                  validator: (String value) {
                    if (value.isEmpty) {
                      return 'Category Name cannot be blank!';
                    }
                    return value.contains('') ? 'Do not use the @ char.' : null;
                  },
                ))
              ],
            ),
            actions: [
              FlatButton(
                child: Text('Ok'),
                onPressed: () {
                  final newCategory =
                      Category(name: _todoDescriptionFormController.value.text);
                  if (newCategory.name.isNotEmpty) {
                    categoryBloc.addCategory(newCategory);

                    //dismisses the bottomsheet
                    Navigator.pop(context);
                  }
                },
              ),
            ],
          );
        });
  }

  _deleteConfirmDialog(BuildContext context, int id) async {
    return showDialog(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Warning'),
          content: const Text('Are you sure want to delete this item?'),
          actions: [
            FlatButton(
              child: const Text('No'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: const Text('Yes'),
              onPressed: () {
                categoryBloc.deleteCategoryById(id);
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Select Category"),
        ),
        body: Container(
          child: StreamBuilder(
            stream: categoryBloc.category,
            builder:
                (BuildContext context, AsyncSnapshot<List<Category>> snapshot) {
              if (snapshot.hasData) {
                return snapshot.data.length != 0
                    ? ListView.builder(
                        itemCount: snapshot.data.length,
                        itemBuilder: (context, itemPosition) {
                          Category category = snapshot.data[itemPosition];
                          return InkWell(
                            onTap: () {
                              Navigator.pop(context, category);
                            },
                            child: Card(
                                shape: RoundedRectangleBorder(
                                  side: BorderSide(
                                      color: Colors.grey[200], width: 0.5),
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                color: Colors.white,
                                child: ListTile(
                                  title: Text(
                                    category.name,
                                    style: TextStyle(
                                        fontSize: 16.5,
                                        fontFamily: 'RobotoMono',
                                        fontWeight: FontWeight.w500,
                                        decoration: TextDecoration.none),
                                  ),
                                  trailing: InkWell(
                                    onTap: () {
                                      _deleteConfirmDialog(
                                          context, category.id);
                                    },
                                    child: Icon(Icons.delete),
                                  ),
                                )),
                          );
                        },
                      )
                    : Container(
                        child: Center(
                        //this is used whenever there 0 Todo
                        //in the data base
                        child: Container(
                          child: Text(
                            "Add Category",
                            style: TextStyle(
                                fontSize: 19, fontWeight: FontWeight.w500),
                          ),
                        ),
                      ));
              } else {
                return Center(
                  /*since most of our I/O operations are done
        outside the main thread asynchronously
        we may want to display a loading indicator
        to let the use know the app is currently
        processing*/
                  child: loadingData(),
                );
              }
            },
          ),
        ),
        floatingActionButton: Padding(
          padding: EdgeInsets.only(bottom: 25),
          child: FloatingActionButton(
            elevation: 5.0,
            onPressed: () {
              _showAddCategorySheet(context);
            },
            backgroundColor: Colors.white,
            child: Icon(
              Icons.add,
              size: 32,
              color: Colors.indigoAccent,
            ),
          ),
        ));
  }

  Widget loadingData() {
    //pull todos again
    categoryBloc.getCategory();
    return Container(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            CircularProgressIndicator(),
            Text("Loading...",
                style: TextStyle(fontSize: 19, fontWeight: FontWeight.w500))
          ],
        ),
      ),
    );
  }
}
