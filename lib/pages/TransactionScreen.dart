import 'package:expense_manager/bloc/transactionBloc.dart';
import 'package:expense_manager/model/category.dart';
import 'package:expense_manager/model/transaction.dart';
import 'package:expense_manager/pages/CategoryScreen.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class TransactionScreen extends StatefulWidget {
  final int id;
  TransactionScreen({this.id});

  @override
  _TransactionScreenState createState() => _TransactionScreenState();
}

class _TransactionScreenState extends State<TransactionScreen> {
  final _formKey = GlobalKey<FormState>();
  final TransactionBloc transactionBloc = TransactionBloc();

  TextEditingController _textDateEditingController = TextEditingController();
  TextEditingController _textCategoryEditingController =
      TextEditingController();
  TextEditingController _textAmountEditingController = TextEditingController();
  TextEditingController _textDescriptionEditingController =
      TextEditingController();

  DateTime selectedDate = DateTime.now();

  bool isExpense = true;
  bool isNew = true;

  Future<void> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        _textDateEditingController.text =
            DateFormat('dd/MMM/yyyy').format(selectedDate).toString();
      });
  }

  @override
  void initState() {
    _textDateEditingController.text =
        DateFormat('dd/MMM/yyyy').format(selectedDate).toString();
    if (widget.id != null) {
      setState(() {
        isNew = false;
      });
      getData();
    }
    super.initState();
  }

  getData() async {
    print(widget.id);
    Transaction _transaction =
        await transactionBloc.getSingleTransaction(widget.id);
    print("KUDUNE ENEK DATA");
    print(_transaction.description);

    setState(() {
      _textDateEditingController.text = _transaction.salesDate;
      _textCategoryEditingController.text = _transaction.categoryName;
      _textAmountEditingController.text = _transaction.amount;
      _textDescriptionEditingController.text = _transaction.description;
      isExpense = _transaction.status == "expense" ? true : false;
    });
  }

  _deleteConfirmDialog(BuildContext context, int id) async {
    final pilihan = await showDialog(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Warning'),
          content: const Text('Are you sure want to delete this item?'),
          actions: [
            FlatButton(
              child: const Text('No'),
              onPressed: () {
                Navigator.of(context).pop(false);
              },
            ),
            FlatButton(
              child: const Text('Yes'),
              onPressed: () {
                transactionBloc.deleteTransactionById(id);
                Navigator.of(context).pop(true);
              },
            )
          ],
        );
      },
    );
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Create Transaction"),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(5.0),
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ButtonTheme(
                    minWidth: MediaQuery.of(context).size.width / 2.1,
                    height: 40.0,
                    child: isExpense
                        ? RaisedButton(
                            onPressed: () {
                              setState(() {
                                isExpense = true;
                              });
                            },
                            child: Text(
                              "EXPENCE",
                              style: TextStyle(color: Colors.white),
                            ),
                          )
                        : OutlineButton(
                            child: new Text("EXPENCE"),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(0.0),
                                side: BorderSide(color: Colors.red)),
                            onPressed: () {
                              setState(() {
                                isExpense = true;
                              });
                            },
                          ),
                  ),
                  ButtonTheme(
                    minWidth: MediaQuery.of(context).size.width / 2.1,
                    height: 40.0,
                    child: !isExpense
                        ? RaisedButton(
                            onPressed: () {
                              setState(() {
                                isExpense = false;
                              });
                            },
                            child: Text(
                              "INCOME",
                              style: TextStyle(color: Colors.white),
                            ),
                          )
                        : OutlineButton(
                            child: new Text("INCOME"),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(0.0),
                                side: BorderSide(color: Colors.red)),
                            onPressed: () {
                              setState(() {
                                isExpense = false;
                              });
                            },
                          ),
                  )
                ],
              ),
              Form(
                  key: _formKey,
                  child: Column(children: <Widget>[
                    TextField(
                      onTap: () {
                        _selectDate(context);
                      },
                      readOnly: true,
                      controller: _textDateEditingController,
                      decoration:
                          InputDecoration(labelText: "Transaction Date"),
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    TextField(
                      onTap: () async {
                        Category category = await Navigator.push(
                          context,
                          // Create the SelectionScreen in the next step.
                          MaterialPageRoute(
                              builder: (context) => CategoryScreen()),
                        );
                        _textCategoryEditingController.text = category.name;
                      },
                      readOnly: true,
                      controller: _textCategoryEditingController,
                      decoration: InputDecoration(hintText: "Select Category"),
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    TextFormField(
                      controller: _textAmountEditingController,
                      decoration: InputDecoration(hintText: 'Amount'),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Amount cannot be blank';
                        }
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 15.0,
                    ),
                    TextFormField(
                      controller: _textDescriptionEditingController,
                      decoration: InputDecoration(hintText: 'Description'),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Description cannot be blank';
                        }
                        return null;
                      },
                    ),
                    Container(
                      margin: EdgeInsets.all(20),
                      width: MediaQuery.of(context).size.width,
                      child: isNew
                          ? FlatButton(
                              child: Text('Save'),
                              color: Colors.blueAccent,
                              textColor: Colors.white,
                              onPressed: () {
                                if (_formKey.currentState.validate()) {
                                  _formKey.currentState.save();
                                  Transaction newTransaction = Transaction(
                                      categoryName:
                                          _textCategoryEditingController.text,
                                      amount: _textAmountEditingController.text,
                                      description:
                                          _textDescriptionEditingController
                                              .text,
                                      salesDate:
                                          _textDateEditingController.text,
                                      status: isExpense ? 'expense' : 'income');
                                  transactionBloc
                                      .addTransaction(newTransaction);
                                  Navigator.pop(context);
                                }
                              },
                            )
                          : Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width / 2.5,
                                  child: FlatButton(
                                      child: Text('Delete'),
                                      color: Colors.red,
                                      textColor: Colors.white,
                                      onPressed: () {
                                        _deleteConfirmDialog(
                                            context, widget.id);
                                      }),
                                ),
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width / 2.5,
                                  child: FlatButton(
                                    child: Text('Save'),
                                    color: Colors.blueAccent,
                                    textColor: Colors.white,
                                    onPressed: () {
                                      if (_formKey.currentState.validate()) {
                                        _formKey.currentState.save();
                                        Transaction newTransaction = Transaction(
                                            id: widget.id,
                                            categoryName:
                                                _textCategoryEditingController
                                                    .text,
                                            amount: _textAmountEditingController
                                                .text,
                                            description:
                                                _textDescriptionEditingController
                                                    .text,
                                            salesDate:
                                                _textDateEditingController.text,
                                            status: isExpense
                                                ? 'expense'
                                                : 'income');
                                        transactionBloc
                                            .updateTransaction(newTransaction);
                                        Navigator.pop(context);
                                      }
                                    },
                                  ),
                                )
                              ],
                            ),
                    ),
                  ]))
            ],
          ),
        ),
      ),
    );
  }
}
