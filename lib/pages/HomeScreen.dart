import 'package:expense_manager/bloc/transactionBloc.dart';
import 'package:expense_manager/model/transaction.dart';
import 'package:expense_manager/pages/TransactionScreen.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  TransactionBloc transactionBloc = TransactionBloc();

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  getBalance(List<Transaction> snapshot) {
    int balance = 0;
    snapshot.forEach((element) {
      print(element.status);
      if (element.status == "expense") {
        balance -= int.parse(element.amount);
      } else {
        balance += int.parse(element.amount);
      }
    });
    return balance.toString();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Expense Manager"),
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 50.0,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10),
                        bottomLeft: Radius.circular(10),
                        bottomRight: Radius.circular(10)),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 1,
                        blurRadius: 9,
                        offset: Offset(0, 2), // changes position of shadow
                      ),
                    ],
                  ),
                  child: StreamBuilder(
                      stream: transactionBloc.todos,
                      builder: (BuildContext context,
                          AsyncSnapshot<List<Transaction>> snapshot) {
                        if (snapshot.hasData) {
                          return Center(
                            child: Text(
                                "Balance Rp. ${getBalance(snapshot.data)}"),
                          );
                        } else {
                          return Center(
                            child: Text("Calculating Balance..."),
                          );
                        }
                      }),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Container(
                  height: MediaQuery.of(context).size.height,
                  child: StreamBuilder(
                    stream: transactionBloc.todos,
                    builder: (BuildContext context,
                        AsyncSnapshot<List<Transaction>> snapshot) {
                      if (snapshot.hasData) {
                        return snapshot.data.length != 0
                            ? ListView.builder(
                                itemCount: snapshot.data.length,
                                itemBuilder: (context, itemPosition) {
                                  Transaction transaction =
                                      snapshot.data[itemPosition];
                                  return InkWell(
                                    onTap: () async {
                                      final result = await Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                TransactionScreen(
                                                  id: transaction.id,
                                                )),
                                      );
                                      transactionBloc.getTransaction();
                                    },
                                    child: Container(
                                      width: MediaQuery.of(context).size.width,
                                      height: 75.0,
                                      margin: EdgeInsets.only(bottom: 10.0),
                                      decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(10),
                                            topRight: Radius.circular(10),
                                            bottomLeft: Radius.circular(10),
                                            bottomRight: Radius.circular(10)),
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.5),
                                            spreadRadius: 1,
                                            blurRadius: 9,
                                            offset: Offset(0,
                                                2), // changes position of shadow
                                          ),
                                        ],
                                      ),
                                      child: ListTile(
                                        contentPadding: EdgeInsets.only(
                                            top: 5.0,
                                            bottom: 10.0,
                                            left: 10.0,
                                            right: 10.0),
                                        leading: Icon(
                                          transaction.status == "expense"
                                              ? Icons.arrow_downward
                                              : Icons.arrow_upward,
                                          color: transaction.status == "expense"
                                              ? Colors.red
                                              : Colors.green,
                                          size: 40,
                                        ),
                                        title:
                                            Text("Rp. ${transaction.amount}"),
                                        subtitle: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text("${transaction.categoryName}"),
                                            Text("${transaction.description}")
                                          ],
                                        ),
                                        trailing:
                                            Text("${transaction.salesDate}"),
                                      ),
                                    ),
                                  );
                                },
                              )
                            : Container(
                                child: Center(
                                //this is used whenever there 0 Todo
                                //in the data base
                                child: Container(
                                  child: Text(
                                    "Start add Transaction",
                                    style: TextStyle(
                                        fontSize: 19,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ),
                              ));
                      } else {
                        return Center(
                          /*since most of our I/O operations are done
        outside the main thread asynchronously
        we may want to display a loading indicator
        to let the use know the app is currently
        processing*/
                          child: loadingData(),
                        );
                      }
                    },
                  ),
                )
              ],
            ),
          ),
        ),
        floatingActionButton: new FloatingActionButton(
            elevation: 0.0,
            child: new Icon(Icons.add),
            onPressed: () async {
              final result = await Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => TransactionScreen()),
              );
              transactionBloc.getTransaction();
            }));
  }

  Widget loadingData() {
    //pull todos again
    transactionBloc.getTransaction();
    return Container(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            CircularProgressIndicator(),
            Text("Loading...",
                style: TextStyle(fontSize: 19, fontWeight: FontWeight.w500))
          ],
        ),
      ),
    );
  }
}
