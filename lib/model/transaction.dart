class Transaction {
  int id;
  String amount;
  String salesDate;
  String description;
  String categoryName;
  String status;
  Transaction(
      {this.id,
      this.amount,
      this.salesDate,
      this.description,
      this.categoryName,
      this.status});
  factory Transaction.fromDatabaseJson(Map<String, dynamic> data) =>
      Transaction(
          id: data['id'],
          description: data['description'],
          amount: data['amount'],
          salesDate: data['salesDate'],
          categoryName: data['categoryName'],
          status: data['status']);
  Map<String, dynamic> toDatabaseJson() => {
        "id": this.id,
        "description": this.description,
        "amount": this.amount,
        "salesDate": this.salesDate,
        "categoryName": this.categoryName,
        "status": this.status
      };
}
