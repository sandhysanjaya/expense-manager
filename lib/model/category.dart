class Category {
  int id;
  String name;
  Category({this.id, this.name});
  factory Category.fromDatabaseJson(Map<String, dynamic> data) =>
      Category(id: data['id'], name: data['name']);
  Map<String, dynamic> toDatabaseJson() => {"id": this.id, "name": this.name};
}
